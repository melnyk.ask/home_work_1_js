// 1.Як можна оголосити змінну у Javascript?
// let x = 10;
// let y = "ten";
// let t;
// t = 101;
// const y_length = 3;
// const day_of_birth = 'seven of march';

// У чому різниця між функцією prompt та функцією confirm?
// prompt - повертає рядок, який ввів каристувач. Або null, якща користувач натисне "Скасувати".
// confirm - повертає або true, або false. В залежності від відповіді, обраної користуачем.

// Що таке неявне перетворення типів? Наведіть один приклад.
// Неявне перетворення типів даних - це перетворення типів, яке виконується операторами, функціями JS автоматично,
// використовуючи "свою" логіку.
// Наприклад, при виконанні операції додавання двох змінних (number та string) буде виконане неявне перетворення типу
// даних (змінної з типом number на string). Результат буде - string.
// let a = 10;
// let b = "3";
// let c = a + b;


// 1
let admin;
let my_name = "Andriy";
admin = my_name;
console.log(admin);

// 2
let days = 1;
let hours = 24;
let mins = 60;
let seconds = 60;
let result = days * hours * mins * seconds;
console.log(result);

// 3
let answer = prompt("Are you OK?");
console.log(answer);
